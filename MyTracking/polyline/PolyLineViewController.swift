//
//  PolyLineViewController.swift
//  MyTracking
//
//  Created by apple on 20/07/21.
//

import UIKit
import GooglePlaces
import GoogleMaps
import Alamofire
import SwiftyJSON
import Polyline
class PolyLineViewController: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    let sourceLat = 10.8318
    let sourceLng = 78.6934
    
    let dLoc = 10.7986
    let dLng = 78.6804
    
    var stepsCoords:[CLLocationCoordinate2D] = []
    var marker:GMSMarker?
    var iPosition:Int = 0;
    var timer = Timer()
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let sourceLocation = "\(sourceLat),\(sourceLng)"
        let distinationLoction = "\(dLoc),\(dLng)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceLocation)&destination=\(distinationLoction)&key=AIzaSyCXTBMEOgifVf3smX1vwwKr7UC5yEiPDPw"
        Alamofire.request(url).responseJSON { (response) in
            guard let data = response.data else {
                return
            }
            do{
                let jsonData = try JSON(data: data)
                let routes = jsonData["routes"].arrayValue
                for route in routes{
                    print("poly",routes)
                    let overviewPolyline = route["overview_polyline"].dictionary
                   // let path = GMSPath(fromEncodedPath: overviewPolyline!["points"] as! String)
                    let points = overviewPolyline?["points"]?.string
                   // self.stepsCoords = decodePolyline(overviewPolyline!["points"] as! String)!
                        let path = GMSPath.init(fromEncodedPath: points ?? "")
                        let polyline = GMSPolyline.init(path: path)
                        polyline.strokeWidth = 6
                    polyline.strokeColor = .systemBlue
                        polyline.map = self.mapView
                        
                        
                        
                    
                }
                
            }
            catch let error {
                print(error.localizedDescription)
            }
        }
        let sourceMarker = GMSMarker()
        sourceMarker.position = CLLocationCoordinate2D(latitude: sourceLat, longitude: sourceLng)
        sourceMarker.title = "Manapparai"
        //sourceMarker.description = "Sub district of Trichy"
        sourceMarker.map = self.mapView
        // Do any additional setup after loading the view.
        let distinationMarker = GMSMarker()
        distinationMarker.position = CLLocationCoordinate2D(latitude: dLoc, longitude: dLng)
        distinationMarker.title = "Manapparai"
        //sourceMarker.description = "Sub district of Trichy"
        distinationMarker.map = self.mapView
        
        let camera = GMSCameraPosition(target: sourceMarker.position, zoom: 15)
        mapView.animate(to: camera)
       // self.stepsCoords = overviewPolyline?["points"]?.string
        self.timer = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: true, block: { (_) in
//            self.playAnimation()
          //  self.moveMarker()
        })
        
        RunLoop.current.add(self.timer, forMode: RunLoop.Mode.common)
    }
    func moveMarker(){
       // self.lat += 0.0017
        CATransaction.begin()
        CATransaction.setValue(2.0, forKey: kCATransactionAnimationDuration)
        CATransaction.setCompletionBlock {
            self.marker?.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        }
        self.mapView.animate(to: GMSCameraPosition.camera(withLatitude: sourceLat, longitude: sourceLng, zoom: 15))
        self.marker?.position = CLLocationCoordinate2D(latitude: dLoc, longitude: dLoc)
        CATransaction.commit()
        marker?.map = self.mapView
    }
    func playAnimation(){
        if iPosition <= self.stepsCoords.count - 1 {
            let position = self.stepsCoords[iPosition]
            self.marker?.position = position
             mapView.camera = GMSCameraPosition(target: position, zoom: 15, bearing: 0, viewingAngle: 0)
            self.marker?.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
            if iPosition != self.stepsCoords.count - 1 {
                self.marker?.rotation = CLLocationDegrees(exactly: self.getHeadingForDirection(fromCoordinate: self.stepsCoords[iPosition], toCoordinate: self.stepsCoords[iPosition+1]))!
            }
            
            
            if iPosition == self.stepsCoords.count - 1 {
                iPosition = 0;
                timer.invalidate()
            }
            
            iPosition += 1
    }
        
        
    }
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
        
        let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        if degree >= 0 {
            return degree - 180.0
        }
        else {
            return (360 + degree) - 180
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

