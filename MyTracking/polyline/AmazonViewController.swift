//
//  AmazonViewController.swift
//  MyTracking
//
//  Created by apple on 20/07/21.
//

import UIKit

class AmazonViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnAction(_ sender: Any) {
        openAmazonProduct(withId: "B07D3QM4SM/ref=cm_sw_r_oth_api_glc_fabc_VGWQYHHRS1J4KZPYFQRT")
    }
    func openAmazonProduct(withId id: String) {

       guard let amazonWebURL = URL(string: "https://amzn.to/2MQC8Bz"),
             let amazonAppURL = URL(string: "com.amazon.mobile.shopping://www.amazon.com/products/\(id)/") else {
                 return
       }
            
       if UIApplication.shared.canOpenURL(amazonAppURL) {
                UIApplication.shared.open(amazonAppURL, options: [:], completionHandler: nil)
       }
       else if UIApplication.shared.canOpenURL(amazonWebURL) {
                UIApplication.shared.open(amazonWebURL, options: [:], completionHandler: nil)
       }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
